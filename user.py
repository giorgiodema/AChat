
import requests
import json


class User():

	url = 'http://127.0.0.1:5984/chat_users/'

	@staticmethod
	def get_id(username):
		r=requests.get(User.url+'_design/all/_view/id_rev?key='+'"'+username+'"')
		if r.json()['rows']:
			return r.json()['rows'][0]['value'][0]
		else:
			return r.json()['rows']
	
	@staticmethod
	def get_rev(username):
		r=requests.get(User.url+'_design/all/_view/id_rev?key='+'"'+username+'"')
		return r.json()['rows'][0]['value'][1]

	@staticmethod
	def get_user(user_id):
		r=requests.get(User.url+user_id)
		return r.json()

	@staticmethod
	def get_profile_pic(username):
		r=requests.get(User.url+'_design/all/_view/profile_pic?key='+'"'+username+'"')
		return r.json()['rows'][0]['value']

	@staticmethod
	def insert_profile_pic(username, picture):
		user_id= User.get_id(username)
		payload = User.get_user(user_id)
		payload['profile_pic'] = picture 
		r=requests.put(User.url+user_id, data =json.dumps(payload))

	@staticmethod
	def get_contacts(username):
		r=requests.get(User.url+'_design/all/_view/contacts?key='+'"'+username+'"')
		if r.json()['rows']:
			return r.json()['rows'][0]['value']
		else:
			return r.json()['rows']
	
	@staticmethod
	def get_single_contact(username, user):       
		contacts = User.get_contacts(username)   
		for item in contacts:                    
			if item == user:
				return user
		return None

	@staticmethod
	def add_contact(username, user):
		user_id= User.get_id(username)
		payload = User.get_user(user_id)
		payload['contacts'].append(user) 
		r=requests.put(User.url+user_id, data =json.dumps(payload))
		
	@staticmethod
	def user_login(username, password):
		#cerco utente tramite l'username
		r=requests.get(User.url+'_design/all/_view/login?key='+'"'+username+'"') 
		if r.json()['rows']:#se non c'e' restituisce una lista vuota che e' considerata False
			if r.json()['rows'][0]['value'] == password:
				#l'utente ha inserito username e password giuste carico
				user_id = User.get_id(username)
				return User.get_user(user_id)
			else:
				# password sbagliata!
				return 0
		else:
			#l'utente non e' nel db
			return -1

	@staticmethod
	def create_user(username, password, language):
		user_id = requests.get("http://127.0.0.1:5984/_uuids").json()['uuids'][0]
		payload = {"username":username,"password":password,"language":language, "profile_pic":"", "contacts":[]}
		r=requests.put(User.url+user_id , data=json.dumps(payload))
		if r.json().has_key('ok'):
			return True
		else:
			return False

	@staticmethod
	def modify_language(username, language): #(inserisce e modifica)
		user_id= User.get_id(username)
		payload = User.get_user(user_id)
		payload['language'] = language 
		r=requests.put(User.url+user_id, data =json.dumps(payload))








