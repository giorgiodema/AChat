from flask import Flask,request,render_template,session,redirect
import random
import string
import os
import httplib2

import requests

from flask_socketio import SocketIO,send,emit

#Google Apis libraries
from oauth2client.client import flow_from_clientsecrets


""" Google Oauth2 fase1 """#prendo i parametri dal file client_secrets.json e creo oggetto flow
flow = flow_from_clientsecrets('./client_secrets.json',
                               scope='https://www.googleapis.com/auth/contacts.readonly',
                               redirect_uri='http://127.0.0.1:5050/import_contacts_from_google/response')

app = Flask(__name__)




from user import *

login_server_id = "secret_id"
chat_server_addr = "http://127.0.0.1:5000"



@app.route('/',methods=['GET'])
def index():
	return render_template('login.html',error=None);



@app.route('/login',methods=['POST'])
def login():
	
	u =	request.form["username"]
	p = request.form["password"]
	
	ret = User.user_login(u,p)
	if ret == 0:
		print "login:::password sbagliata!"
		return render_template('login.html',error="Invalid Username or Password")
		
	elif ret == -1:
		print "login:::username sbagliato!"
		return render_template('error.html',error="Invalid Username or Password")
		
	else:
		print "login:::utente trovato nel db"
		token = ''.join(random.sample(string.lowercase,10))
		l = ret['language']
		c = ret['contacts']
		payload ={"username": u,"token" :token,"language": l,"contacts": c}
		
		req = {"username":u,"language":l,"token":token}
		r = requests.post(chat_server_addr+"/"+login_server_id,req)
		
		print "login:::r:"+str(r.text)
		if r.text == 'OK':
			session["username"] = u
			return render_template('chat.html',data=payload)
		else:
			return render_template('error.html',error="Internal Error, please try later")
	
@app.route('/register',methods=['POST'])
def register():
	u =	request.form["username"]
	p = request.form["password"]
	l = request.form["language"]
	
	ret = User.create_user(u, p, l)
	token = ''.join(random.sample(string.lowercase,10))
	
	payload = {"username": u,"token" :token,"language": l,"contacts": []}
	req = {"username":u,"language":l,"token":token}
	r = requests.post(chat_server_addr+"/"+login_server_id,req)
	if r.text == "OK":
		session["username"] = u
		return render_template('chat.html',data=payload)
	else:
		return render_template("error.html")

#If the user has previously granted your application access, the authorization 
#server immediately redirects again to redirect_uri.
#If they grant access, they get redirected to redirect_uri with a code query 
#string parameter.
@app.route('/import_contacts_from_google')
def import_contacts_request():
	""" Google Oauth2 fase2 """
	auth_uri = flow.step1_get_authorize_url()# Redirect the user to auth_uri on your platform.
	print "import_contacts_request:::auth_uri:"+str(auth_uri)
	return redirect(auth_uri)


# Una dopo che l'utente ha garantito l'accesso ai suoi contatti viene reindirizzato
# alla pagina specificata dall'applicazione al momento della registrazione:
# http://127.0.0.1:5050/import_contacts_from_google/response
# il corpo della richiesta conterrà un codice che verrà scambiato con l'access_token.

@app.route('/import_contacts_from_google/response')
def import_contacts_response():
	code = request.args.get("code","")
	print "import_contacts_response:::code:"+code
	""" Google Oauth2 fase3 """
	credentials = flow.step2_exchange(code)
	print "import_contacts_response:::credentials:"+str(credentials)

	""" inserisce header con codice in tutte le richieste fatte tramite oggetto http """
	http = httplib2.Http()
	http = credentials.authorize(http)

	# con l'access_token posso richiedere i contatti dell'utente
	ret = http.request('https://people.googleapis.com/v1/people/me/connections?personFields=emailAddresses') #richiedo solo le email dei contatti
	for item in json.loads(ret[1])["connections"]:

		if "emailAddresses" in item.keys():
			google_user = item["emailAddresses"][0]["value"]
			print "\ngoogle_user:::"+str(google_user)
			u_id = User.get_id(google_user) #verifico se il contatto google e' nel DB
			if u_id: #se c'e' lo aggiungo ai contatti dell'utente
				if google_user not in User.get_contacts(session["username"]):#verifico che non sia gia' nei contatti dell'utente
					User.add_contact(session["username"], google_user)

	data = json.loads(ret[1])
	while "nextPageToken" in data:
		print "\n\nRichiedo la prossima pagina\n\n"
		next_token = data["nextPageToken"]
		ret = http.request('https://people.googleapis.com/v1/people/me/connections?personFields=emailAddresses&pageToken='+next_token)
		data = json.loads(ret[1])
		for item in data["connections"]:
			if "emailAddresses" in item.keys(): #if len(item) == 3:		
				google_user = item["emailAddresses"][0]["value"]
				print "\ngoogle_user:::"+str(google_user)
				u_id = User.get_id(google_user) #verifico se il contatto google e' nel DB
				if u_id: #se c'e' lo aggiungo ai contatti dell'utente
					if google_user not in User.get_contacts(session["username"]):#verifico che non sia gia' nei contatti dell'utente
						User.add_contact(session["username"], google_user)


	


	new_contacts = User.get_contacts(session["username"])
	print "import_contact_response:::new_contacts:"+str(new_contacts);
	

	user_id = User.get_id(session["username"])
	#print "user_id:"+str(user_id)
	
	user_info = User.get_user(user_id)
	print "user_info:"+str(user_info)
	
	username = user_info['username']
	language = user_info['language']	
	token = ''.join(random.sample(string.lowercase,10))
	
	payload ={"username": username,"token" :token,"language": language,"contacts": new_contacts}
	
	req = {"username":username,"language":language,"token":token}
	r = requests.post(chat_server_addr+"/"+login_server_id,req)

	if r.text == 'OK':
		return render_template('chat.html',data=payload)
	else:
		return render_template('error.html')





app.secret_key = os.urandom(24)
app.run(port=5050)


