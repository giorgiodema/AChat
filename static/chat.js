/* ----------------------OBJECTS DEFINITIONS -------------------------*/

function Group(name,members){
	this.name = name;
	this.members = members;
}

/* ------------------------------------------------------------------ */

/* ----------------------GLOBAL VARIABLES ----------------------------*/

// informazioni dell'utente, prese dai metadati del  documento html 
var username="";
var token="";
var language="";

// array di oggetti Group, contiene informazioni su tutti i gruppi attivi
var groups = [];

// {name:"c_name",status:"c_status"}
var contacts = [];

// array di stringhe, sono i nomi delle chat attive
var active_chats = [];



var selected_contact_name = [];
console.log("selected_contact_name: "+selected_contact_name);

var current_chat=null;

var socket = io.connect('http://localhost:5000');

/* ------------------------------------------------------------------ */


/*-----------------------FUNCTIONS------------------------------------*/

// callback function, called by the 'message' event socketio handler
function createNewChat(chat_name,visibility){

	document.getElementById("chat_div").innerHTML = document.getElementById("chat_div").innerHTML + '<ul class="show_messages" id="'+chat_name+'message_box'+'"></ul>';
	document.getElementById(chat_name+'message_box').style.visibility=visibility;
	document.getElementById("group_list").innerHTML = document.getElementById("group_list").innerHTML + '<div class="group_list_item" id="'+chat_name+'group_list'+'" onmouseover=showMembers('+'"'+chat_name+'"'+');  onmouseout=hideMembers('+'"'+chat_name+'"'+')>\
																													<p>'+chat_name+'</p>\
																													<input type="image"  src="../static/icons/close.png" onclick=closeChat("'+chat_name  +'")>\
																										</div>';          
	
	var group = null;
	for(var i=0;i<groups.length;i++)
		if(groups[i].name==chat_name)
			group = groups[i]
			
	if(group!=null){
		var members_list = "<ul class='members_list' id="+chat_name+"members_list>";
		for(var i=0;i<group.members.length;i++)
			members_list += "<li><h3>"+group.members[i]+ "</h3></li>";
		members_list += "</ul>";
		document.getElementById("chat_div").innerHTML +=members_list;
	}
	
	

}
// callback function, called by the 'message' event socketio handler
function addMessageToChat(chat_name,from,message){
	var message_box = document.getElementById(chat_name+"message_box");
	message_box.innerHTML += '<li class="received_message"><h3 class="from">'+from+'</h3><p class="message">'+message+'</p></li>';
}

function contains(array,elem){
	for(var i=0;i<array.length;i++)
		if(array[i]==elem)
			return true;
	return false;
}
/* ------------------------------------------------------------------ */


$(document).ready(function(){
	


	/*
	 * Carica i contatti in contacts
	 */
	var aux = document.getElementById("contacts_list").children;
	for(var i=0; i<aux.length; i++){
		contacts.push({name:aux[i].children[1].innerText, status:"offline"});
	}

	
	
	
	
	/*
	 * Aggiunge l'username del contatto selezionato dall'utente in "contact_selected"
	 */
	$("li.contact").click(function(){
		
		var contact = $(this)[0]; // get the current element
		
		var contact_name = $(":nth-child(2)",this).text();
		if(!contains(selected_contact_name,contact_name)){
			var status = contact.childNodes[4].innerText;
			console.log("li.contact.click:::status:"+status);
			if(status=="online"){
				console.log("li.contact.click:::selected contact: "+contact_name);
				contact.style.backgroundColor = "#d9d9d9" // evidenzia il contatto selezionato
				selected_contact_name.push(contact_name);
			}
		}else{
			contact.style.backgroundColor = "#ffffff"; // ripristina il colore del contatto
			var aux = [];
			for(var i=0;i<selected_contact_name.length;i++){
				if(selected_contact_name[i]!=contact_name)
					aux.push(selected_contact_name[i]);
			}
			selected_contact_name = aux;
		}
	});
	
	
	
	
	/*
	 * Ogni 5secondi l'host
	 * richiede al server lo "status" dei suoi contatti
	 */
	function contactsInfo(){
		console.log("left_div:mouseenter");
		
		
		var json_msg = JSON.stringify(contacts);
		socket.emit('contacts_info',json_msg);
	}
	
	window.setInterval(contactsInfo,5000);
	
	
	
	


	
	
	
	
	
	
	
	
	/* Se contact_selected è vuoto apre una alert con un messaggio di errore. 
	 * Altrimenti Crea una nuova chat:
	 * 						--> individuale: se contact_selected ha un solo elemento
	 * 						--> di gruppo  : se contact_selected ha più elementi
	 */
	 
	$("div.start_chat").click(function(){
		
//-------------------- STYLE -------------------------------------------
		// rirpistina il colore degli elementi selezionati
		var contacts = document.getElementsByClassName("contact");
		for(var i=0;i<contacts.length;i++)
		contacts[i].style.backgroundColor = "#ffffff";
//----------------------------------------------------------------------
		// chat di gruppo
		if(selected_contact_name.length>1){
			
			// richiede all'utente di inserire il nome del gruppo
			var g = new Group(prompt("Insert group name:", "group name"),[]);
			if (g.name == null || g.name == "" || contains(active_chats,g.name)) {
				// TODO error handling
			} else {

				
				g.members = [];
				for(var k=0;k<selected_contact_name.length;k++)
					g.members.push(selected_contact_name[k]);
					
				groups.push(g);
				active_chats.push(g.name);
				
				// aggiorno documento html
				createNewChat(g.name,"visible");
				
				
				// nasconde la chat corrente
				if(current_chat!=null){
					var old_chat_message_box = document.getElementById(current_chat+'message_box');
					old_chat_message_box.style.visibility="hidden";
					
					var old_chat_group_list = document.getElementById(current_chat+"group_list");
					old_chat_group_list.style.backgroundColor = "#b3b3b3";

				}
				current_chat = g.name;
				active_chats.push(current_chat);
				document.getElementById(current_chat+"group_list").style.backgroundColor = "#999999";
			}
		}
		// chat individuale
		else if(selected_contact_name.length==1){
			
			if(!contains(active_chats,selected_contact_name[0])){
				createNewChat(selected_contact_name[0],"visible");
				// nasconde la chat corrente
				if(current_chat!=null){
					var old_chat_message_box = document.getElementById(current_chat+'message_box');
					old_chat_message_box.style.visibility="hidden";
					
					var old_chat_group_list = document.getElementById(current_chat+"group_list");
					old_chat_group_list.style.backgroundColor = "#b3b3b3";
				}
				current_chat = selected_contact_name[0];
				active_chats.push(current_chat);
				document.getElementById(current_chat+"group_list").style.backgroundColor = "#999999";
			}
		}
		
		// errore
		else{
		}
		
		selected_contact_name = [];
	});
	
	
	
	
	
	
	
	
	
	
	/*
	 * Switch chat
	 */
	$(document).on("click","div.group_list_item",function(){
		selected_chat = $(this).children("p")[0].innerText;
		console.log("div.group_list_item:::selected_group: "+selected_chat);
		
		if(current_chat!=selected_chat){
			if(current_chat!=null){
				var old_chat_message_box = document.getElementById(current_chat+'message_box');
				if(old_chat_message_box!=null)
					old_chat_message_box.style.visibility="hidden";
				var old_chat_group_list = document.getElementById(current_chat+"group_list");
				if(old_chat_group_list!=null)
					old_chat_group_list.style.backgroundColor = "#b3b3b3";
			}
			var new_chat_message_box = document.getElementById(selected_chat+'message_box');
			if(new_chat_message_box!=null){
				new_chat_message_box.style.visibility="visible";
				document.getElementById(selected_chat+"group_list").style.backgroundColor = "#999999";
				
				current_chat = selected_chat;
			}
		}
	});
	
	
	
	
	
	
	
	
	

});


function logout(){
	socket.emit('logout');
	window.location.replace("/");
}


/*
 * Close selected chat
*/
function closeChat(chat_name){ // TODO

	current_chat=null;


	var group_list = document.getElementById("group_list");
	group_list.removeChild(document.getElementById(chat_name+"group_list"));
	
	var chat_div = document.getElementById("chat_div");
	chat_div.removeChild(document.getElementById(chat_name+"message_box"));
	
	var members_list = document.getElementById(chat_name+"members_list");
	if(members_list!=null){
		chat_div.removeChild(members_list);
	}
		

	var new_active_chats= [];
	for(var i=0;i<active_chats.length;i++){
		if(active_chats[i]!=chat_name)
			new_active_chats.push(active_chats[i]);
	}
	active_chats = new_active_chats
	
	var new_groups = [];
	for(var i=0;i<groups.length;i++){
		if(groups[i].name!=chat_name)
			new_groups.push(groups[i]);
	}
	groups = new_groups;
	
	
}


/*
 * Aggiunge il messaggio alla chat corrente e lo invia al server
 */
function sendMessage(){
	
	console.log("send_button.click:::");
	
	var message_text = document.getElementById("message_text").value;
	document.getElementById("message_text").value = "";
	
	if(message_text!=""){
		console.log("send_button.click:::message:"+message_text);
		
		// aggiunge il messaggio alla chat corrente
		document.getElementById(current_chat+"message_box").innerHTML +='\
						<li class="sent_message">\
							<p class="message">'+message_text+'</p>\
						</li>';
						
		// manda il messaggio al server
		var current_group=null;
		for(var i=0;i<groups.length;i++){
			if(groups[i].name==current_chat)
				current_group = groups[i];
		}
		// messaggio individuale
		if(current_group==null){
			var msg = 	{

							"type":"message",
							"data": {
								"group": "",										
								"from": username,
								"to":current_chat,
								"text":escape( message_text	)				
							}

						}
			var json_msg = JSON.stringify(msg);
			socket.emit('message',json_msg);
		}
		else{
			var msg = 	{

							"type":"message",
							"data": {
								"group": current_group.name,										
								"from": username,
								"to":current_group.members,
								"text": escape(message_text	)				
							}

						}
			var json_msg = JSON.stringify(msg);
			socket.emit('message',json_msg);
		}
					
	}
}


/*
 * Mostra e nasconde i membri del gruppo
*/

function showMembers(chat_name){



	console.log("entering:::"+chat_name);
	
	var is_group=false;
	for(var i=0;i<groups.length;i++)
		if(groups[i].name == chat_name)
			is_group=true;
	
	if(is_group){
		//document.getElementById(current_chat+"message_box").style.opacity=0.3;
		var members_list = document.getElementById(chat_name+"members_list");
		members_list.style.visibility = "visible";
		//members_list.style.opacity=1;
	}

}

function hideMembers(chat_name){

	console.log("exiting:::"+chat_name);
	var is_group=false;
	for(var i=0;i<groups.length;i++)
		if(groups[i].name == chat_name)
			is_group=true;
	
	if(is_group){
		//document.getElementById(current_chat+"message_box").style.opacity=1;
		var members_list = document.getElementById(chat_name+"members_list");
		members_list.style.visibility = "hidden";
	}
}



/*---------------------SOCKET-IO-EVENTS------------------------------*/


socket.on('connect', function() {
    console.log('connected to server');
    
	username = document.getElementsByTagName("META")[0].getAttribute("content");
	token = document.getElementsByTagName("META")[1].getAttribute("content"); 
	language = document.getElementsByTagName("META")[2].getAttribute("content");
	
	console.log(username+":"+token+":"+language);
    
    var msg = {
				"username":username,
				"token"   :token
	}
	
	var json_msg = JSON.stringify(msg);
	console.log("authentication:::json_msg="+json_msg);
	
	socket.emit('authentication',json_msg,(response)=>{
		console.log("authentication:::server response:"+response);
	});
    
});




var json_data;


socket.on('message',function(data){
	console.log('message:::received:'+data)
	
	json_data=JSON.parse(data);

	var from = json_data.data.from;
	var to   = json_data.data.to;
	var group= json_data.data.group;
	var members = json_data.data.members;
	var message = json_data.data.text;
	// messaggio di gruppo
	if(group!=""){
		console.log('message:::group message');
		if(contains(active_chats,group)){
			addMessageToChat(group,from,message);
		}
		else{
			g = new Group(group,members);
			groups.push(g);
			active_chats.push(g.name);
			createNewChat(group,"hidden");
			addMessageToChat(group,from,message);
		}
	}
	// messaggio individuale
	else{
		console.log('message:::individual mesasge');
		if(contains(active_chats,from)){
			console.log('message:::chat already exists');
			addMessageToChat(from,from,message);
		}
		else{
			console.log('chat does not exists');
			createNewChat(from,"hidden");

			active_chats.push(from);
			addMessageToChat(from,from,message);
		}
	}
});

// invoked when the chat_server sends the information about the contacts status
socket.on('contacts_info',function(data){
	json_data=JSON.parse(data);
	
	contacts = json_data;
	for(var i=0; i<contacts.length; i++){
		contact_name = contacts[i]["name"];
		var dom_obj = document.getElementById(contact_name);
		dom_obj.childNodes[4].innerText = contacts[i]["status"];
		if(contacts[i]["status"]=="online")
			dom_obj.childNodes[4].setAttribute("class", "contact_online");
		else
			dom_obj.childNodes[4].setAttribute("class", "contact_offline");
			
		
		
	}
});






































































