# AwesomeChat

Il progetto consiste in una web chat con traduzione istantanea.
La chat è strutturata a microservices:
- Il **login_server** si interfaccia con il database, carica i dati dell'utente e importa i contatti da *Google Contacts*
- Il **chat_server** tiene un elenco dei contatti online e gestisce l'inoltro dei messaggi
- Il **translate_server** si occupa di tradurre i messaggi nella lingua del ricevente

Il **login_server** ed il **chat_server** coumunicano attraverso http in fase di autenticazione e a seguito di una richiesta da parte dell'utente di importare i contatti da google.
Il **chat_server** ed il **translate_server** comunicano attraverso RPC, il chat_server (client RPC) invoca una procedura sul translate_server (server RCP) per tradurre i messaggi.

<p align="center">
<img src="./Schema.png" width="100%"/>
</p>

## 1) LogIn e Registrazione
- L'utente si connette al login_server ed effettua il login o la registrazione.
- Se le credenziali sono valide il login_server manda al chat_server le seguenti informazioni:
 - Username
 - Language
 - Token (una sequenza di caratteri generati casualmente)
- successivamente il login_server carica la pagina della chat 
- lo script nella pagina html effettuerà l'upgrade al protocollo WebSocket e si connetterà al chat_server

## 2-3) Autenticazione
Il chat_server memorizza gli utenti attivi utilizzando due dizionari
```python

connected_users = {		"user01":{"sid":"user01sid","token":"fdsjfskldWETFSDG","language":"IT"},
						"user02":{"sid":"user02sid","token":"fabcjsgdfsETFSaG","language":"EN"},
						"user03":{"sid":"user03sid","token":"defgjsgdffaqergG","language":"ES"}
				  }
				  
connected_sid = {	"user01sid":"user01",
					"user02sid":"user02",
					"user03sid":"user03"
				}
```

Quando un utente si connette al login_server quest'ultimo manda al chat_server le informazioni come specificato sopra.
Una volta ricevute le informazioni sul contatto il chat_server aggiungerà il record dell'utente nel dizionario connected_users

Quando l'utente si connette lo script nella pagina della chat invia l'username e il token che ha ricevuto dal login_server.
Il chat_server controlla il token:
- Se coincide con quello memorizzato nel dizionario:
 - Aggiunge il campo sid (session id), ovvero l'identificativo della sessione ai due dizionari
- Altrimenti:
 - Reindirizza l'utente al login_server

## Importa contatti da Google Contacts
La richiesta di importare i contatti da Google Contacts viene gestita dal login_server, il quale tiene traccia degli utenti connessi tramite cookie.
Quando il login_server riceve una richiesta da parte di un utente di importare i contatti da google:
- Richiede all'utente di inserire le credenziali Google, utilizzando il protocollo Oauth2
- Se l'utente acconsente, viene restituito al login_server l'access token per richiedere i suoi contatti
- Il login server richiede i contatti dell'utente a google
- Una volta ricevuti i contatti accede al DB e seleziona (tra i contatti dell'utente) quelli che sono registrati presso AwesomChat
- Aggiunge i contatti selezionati al record dell'utente nel DB
- Ricarica nuovamente la pagina della chat all'utente con la lista dei contatti aggiornata

## Contatti Online/Offline
La chat non prevede il salvataggio sul DB dei messaggi. Per questo motivo è possibile avviare una chat solo con i contatti online.
L'utente ogni 5 secondi richiede al server informazioni sullo "stato" dei suoi contatti.
Il server risponderà con lo stesso messaggio aggiornando il valore del campo status

- Richiesta:
 - event name: contacts_info

```JSON
data: [{"name":"contact01","status":"offline"},...]
```

- Risposta:
 - event name: contacts_info
 
```JSON
data: [{"name":"contact01","status":"offline"},...]
```

## 4) Invio di un messaggio
Ci sono due tipi di messaggio che l'utente invia al chat_server:
Messaggio individuale:
```JSON

{

	"type":"message",
	"data": {
		"group": "",
		"from": "user01",
		"to": "user02",
		"text":"ciao utente02"
	}

}
```

Messaggio di gruppo:
```JSON

{

	"type":"message",
	"data": {
		"group": "group_name",										
		"from": "user01",
		"to":["user02","user03"],
		"text": "testo del messaggio"				
	}

}
```

## 5) Traduzione di un messaggio
###### chat_server
Quando il chat_server riceve un messaggio da un utente, prima di invocare la funzione remota "translate" sul translate_server deve aggiungere le informazioni relative alla lingua degli utenti.
Se il messaggio di partenza era:
```JSON

{

	"type":"message",
	"data": {
		"group": "group_name",										
		"from": "user01",
		"to":["user02","user03"],
		"text": "testo del messaggio"				
	}

}
```

Con l'aggiunta della lingua degli utenti diventa:
```JSON

{
	type: 'message' 
	data: 
		   { from: { user: 'user01', language: 'IT' },
			 group: 'group_name',
			 to: [ 
					{ user: 'user02', language: 'EN' },
					{ user: 'user03', language: 'ES' } 
				 ]
			 text: 'testo del messaggio' 
			} 
}

```

###### translate_server
Il translate_server si occupa della traduzione dei messaggi dalla lingua del mittente alla lingua del destinatario.
Per la traduzione del testo vengono utilizzate le [APIs di Google Translate](https://cloud.google.com/translate/docs/) .

Se il messaggio in input è un messaggio di gruppo viene prodotta in output una lista contenente tanti messaggi quanti sono i membri del gruppo (tranne il mittente).

Per esempio se l'argomento della funzione remota è:

```JSON

{
	type: 'message' 
	data: 
		   { from: { user: 'user01', language: 'IT' },
			 group: 'group_name',
			 to: [ 
					{ user: 'user02', language: 'EN' },
					{ user: 'user03', language: 'ES' } 
				 ]
			 text: 'ciao' 
			} 
}

```

Il valore di ritorno sarà:
```JSON

{
	"translated_messages": [
								{"data": {
											"group":"group_name",
											"members":["user02","user03"],
											"to":"user02",
											"from":"user01",
											"text":"hello"
										 },
								"type":"message"},
								{"data": {
											"group":"group_name",
											"members":["user02","user03"],
											"to":"user03",
											"from":"user01",
											"text":"hola"
										 }
							]
}


```












## DB

Rappresentazione di un utente all'interno del db
```
{
	"username":"user01",
	"passowrd":"pass01",
	
	"name":".."
	"surname":".."
	
	"language":"IT",
	
	"profile_pic":"serialized_profile_picture",
	
	"contacts":[
		"user02",
		"user03",
		"user04"
	]
}
```

# Documentation
## login_server
### Language: [Python2.7](https://www.python.org)
### Web Framework: [Flask](http://flask.pocoo.org/docs/0.12/)
### Libraries:

- Flask (0.12.2)
- Flask-SocketIO (2.9.2)
- google-api-python-client (1.6.4)
- httplib2 (0.10.3)
- requests (2.18.4)

### how to run:
```Batchfile
$ source venv/bin/activate
(venv)$ python login_server.py

```

## chat_server
### Language: [Python2.7](https://www.python.org)
### Web Framework: [Flask](http://flask.pocoo.org/docs/0.12/)
### Libraries:

- Flask (0.12.2)
- Flask-SocketIO (2.9.2)
- httplib2 (0.10.3)
- pika (0.11.2)
- requests (2.18.4)

### how to run:
```Batchfile
$ source venv/bin/activate
(venv)$ export FLASK_APP=chat_server.py
(venv)$ flask run

```

## translate_server
### Language: [NodeJs](https://nodejs.org/en/)

### Libraries:

- amqplib
- xmlhttprequest


### how to run:
```Batchfile
$ node translater.js

```

## NB
Per far funzionare l'applicazione bisogna:
- Registrare l'applicazione sulla [Google Console] (https://www.google.it/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwiOktGNstDYAhVGalAKHaobD80QFggwMAA&url=https%3A%2F%2Fconsole.cloud.google.com%2F%3Fhl%3Dit&usg=AOvVaw2uNIQnUIdAGhUMGB3h7nAg),
dalla quale si ottengono il ClientID e il ClientSecret, che vanno inseriti nei file client_secrets.json e google_credentials.txt
- Registrarsi su [Google Cloud Platform] (https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwjbqdaNstDYAhWDFtMKHV_fBAAYABAAGgJ3Yg&ohost=www.google.it&cid=CAASE-RoFrTzrCQpYMoUVuq-Nx7ER4Y&sig=AOD64_3kJ-joN_oCLEogchxC0qbH2aPZ5w&q=&ved=0ahUKEwiOktGNstDYAhVGalAKHaobD80Q0QwIJQ&adurl=)
(servizio a pagamento) per ottenere il GOOGLE_TRANSLATE_ACCESS_TOKEN e la TRANSLATE_API_KEY da inserire nel file translate_server.js come indicato sotto

```JavaScript
		var options = {
			q: message_to_translate,
			source: language_from,
			target: language_to,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer GOOGLE_TRANSLATE_ACCESS_TOKEN'
			}
		}

		option = JSON.stringify(options);
		xhr.open('POST', 'https://translation.googleapis.com/language/translate/v2?key=API_KEY_TRANSLATER',false);
		xhr.send(option);

```







