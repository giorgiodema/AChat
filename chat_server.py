from flask import Flask, render_template, request
from flask_socketio import SocketIO,send,emit

import json
import requests



from threading import Thread

import pika
import uuid



class TranslateMessageRPC(object):
	
	def __init__(self):
		# mi connetto con il broker che gira in locale
		self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
		self.channel = self.connection.channel()
		
		# creo una nuova coda su cui mi aspetto di ricevere il valore di ritorno dal server
		result = self.channel.queue_declare(exclusive=True)
		self.callback_queue = result.method.queue
		
		# quando arriva la risposta del server sulla callback_queue viene invocato il metodo
		# on_response 
		self.channel.basic_consume(self.on_response, no_ack=True,queue=self.callback_queue)
		
	def on_response(self, ch, method, props, body):
		if self.corr_id == props.correlation_id:
			self.response = body
	
	# una volta invocato, il metodo call  invia i parametri sulla coda "rpc_queue" su cui il
	# server è in ascolto, e si mette in attesa della risposta sulla coda 
	def call(self, message):
		self.response = None
		self.corr_id = str(uuid.uuid4())
		self.channel.basic_publish(
									exchange='',
									routing_key='rpc_queue',
									properties=pika.BasicProperties(
																		reply_to = self.callback_queue,
																		correlation_id = self.corr_id
																	),
									body=message
									)
		while self.response is None:
			self.connection.process_data_events()
		return self.response

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

if __name__ == '__main__':
    socketio.run(app)



""" data structures to keep track of the connected users"""
connected_users = {} #{"username": {"sid":"...","token":"...","language":"..."}}
connected_sid   = {} #{"sid":"username"}


login_server_id= "secret_id"
login_server_addr = "http://127.0.0.1:5050"



"""--------------------------CHAT_SERVER-----------------------------"""

@app.route("/<login_id>", methods=['POST'])
def create_user(login_id):
	
	#print "create_user:::login_id: "+login_id
	
	if login_id!=login_server_id:
		return "Not Authorized"
		
	else:
		username = request.form["username"]
		language = request.form["language"]
		token = request.form["token"]
		
		print "\n\ncreate_user:::"+username+" "+token+" "+language
		if username not in connected_users.keys():
			connected_users[username] = {}
			connected_users[username]["token"] = token
			connected_users[username]["language"] = language
		else:
			connected_users[username]["token"] = token
			connected_users[username]["language"] = language
		
		return "OK"
		
	


@socketio.on('authentication')
def authentication(arg):
	print '\n\nauthentication:::received json: ' + str(arg)
	
	dic = json.loads(arg)
	username = dic["username"]
	token = dic["token"]
	
	if username not in connected_users.keys():
		print "\n\nauthentication:::"+username+" authenticattion failed"
		return "authentication failed"
	
	elif connected_users[username]["token"] == token:
		connected_users[username]["sid"] = request.sid
		connected_sid[request.sid] = username
		print "\n\nauthentication:::"+username+" authenticated"
		return "authenticated"
	else:
		print "\n\nauthentication:::"+username+" authentication failed"
		return "authentication failed"

""" Formato messaggio """
"""
{
	"type":"message",
	"data":{
			"group":"group_name",
			"from":"user01",
			"to":["user02","user03"],
			"text":"hello friends"
			}
}
"""

#Riceve il messaggio dal client, aggiunge le informazioni sulla lingua e
#inoltra il messaggio al translate_server
@socketio.on('message')
def message(arg):
	#print '\n\nmessage:::received json: '+str(arg)
	#print "\n\nmessage:::arg type:"+str(type(arg)) #unicode
	print "\n\nmessage:::arg:"+arg
	msg = json.loads(arg)
	#print "\n\nmessage:::msg type:"+str(type(msg)) #dict
	
	#controllo se il messaggio proviene da un utente registrato
	if msg["data"]["from"] not in connected_users:
		return
	if connected_users[msg["data"]["from"]]["sid"] != request.sid:
		return


	#aggiungo al messaggio la lingua dei contatti
	msg_lang = {}
	msg_lang["type"] = msg["type"]
	msg_lang["data"] = {}
	msg_lang["data"]["group"] = msg["data"]["group"]
	msg_lang["data"]["from"] = {}
	msg_lang["data"]["from"]["user"] = msg["data"]["from"]
	msg_lang["data"]["from"]["language"] = connected_users[msg["data"]["from"]]["language"]
	msg_lang["data"]["text"] = msg["data"]["text"]
	#print "\n\nmessage:::msg text type:"+str(type(msg["data"]["text"])) #unicode
	print "message:::recived text:"+msg["data"]["text"].encode("unicode_escape")
	msg_lang["data"]["to"] = []
	if isinstance(msg["data"]["to"],list):
		for usr in msg["data"]["to"]:
			aux = {}
			aux["user"] = usr
			aux["language"] = connected_users[usr]["language"]
			msg_lang["data"]["to"].append(aux)
	else:
		msg_lang["data"]["to"].append({"user":msg["data"]["to"],"language":connected_users[msg["data"]["to"]]["language"]})
	
	translated_message = TranslateMessageRPC()
	msg_lang_json = json.dumps(msg_lang,ensure_ascii=False)
	result = translated_message.call(msg_lang_json)
	print '\n\nmessage:::result: ' + result
	
	messages = json.loads(result)
	
	for m in messages["translated_messages"]:
		print "message:::"+str(m)
		sid = connected_users[m["data"]["to"]]["sid"]
		print "message:::sending to sid: "+sid
		aux = json.dumps(m,ensure_ascii=False)
		print "message:::sending message:"+aux
		
		emit('message',aux,room=sid)


@socketio.on('contacts_info')
def contacts_info(arg):
	print "\n\ncontacts_info:::received json: "+str(arg)
	
	if request.sid not in connected_sid.keys():
		return
	
	response = []
	msg = json.loads(arg)
	for c in msg:
		if c["name"] in connected_users.keys():
			response.append( {"name":c["name"],"status":"online"}  )
		else:
			response.append( {"name":c["name"],"status":"offline"}  )
	
	json_response = json.dumps(response)
	emit('contacts_info',str(json_response),room = request.sid)
	
	
@socketio.on('logout')
def logout():
	if request.sid not in connected_sid.keys():
		print "\n\nlogout:::user not found"
		return
		
	uname = connected_sid[request.sid]
	del connected_users[uname]
	del connected_sid[request.sid]
	print "\n\nlogout:::username:"+uname
	return




