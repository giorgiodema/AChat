
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var xhr = new XMLHttpRequest();

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		
		/* creo la coda su cui il client invierà i parametri */
		var q = 'rpc_queue';
		ch.assertQueue(q, {durable: false});
		ch.prefetch(1);
		console.log(' [x] Awaiting RPC requests');
		
		/* processo la richiesta del client e invio il valore di ritorno */
		ch.consume(q, function reply(msg) {
			var n = msg.content.toString();
			var r = translate_message(JSON.parse(n));

			var result = JSON.stringify(r);

			ch.sendToQueue(	msg.properties.replyTo, 						// coda dove il client si aspetta la risposta
							new Buffer(result.toString()),
							{correlationId: msg.properties.correlationId}); // richiesta e risposta devono avere lo stesso correlation_id

			ch.ack(msg);
		});
	});
});

function translate_message(msg){
	console.log(msg);
	console.log(msg["data"]["to"]);
	console.log('msg["data"]["to"].length:'+msg["data"]["to"].length);
	var messages = {"translated_messages":[]}
	
	if (msg["data"]["to"].length>1){
		for(var i=0;i<msg["data"]["to"].length;i++){
			var user = msg["data"]["to"][i];
			var aux = {};
			var members = [];
			members.push(msg["data"]["from"]["user"]);
			for (var k=0;k<msg["data"]["to"].length;k++){
				var mem = msg["data"]["to"][k];
				if (mem!=user)
					members.push(mem["user"])
			}
			aux["data"] = {};

			var message_to_translate = unescape(msg["data"]["text"]);
			language_from = msg["data"]["from"]["language"]
			language_to = user["language"]

			var options = {
				q: message_to_translate,
				source: language_from,
				target: language_to,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ya29.c.Elo7Ba6IXmVEmQJNgWNYuglsp4xsCzsDtO9zjCmzWX78vincD-UnN8rR529Q3wUwuJ-3Wly37wxOne3iy2Oug_FBCBpgn-WzNgiZDkQ3NQa5209KesNqi_LR9S8'
				}
			}

			option = JSON.stringify(options);
			xhr.open('POST', 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyBAO0FxZ1K8dHSDvRO29gWWn03gsjUTwkg',false);
			xhr.send(option);
			console.log("\ngoogle translate response:\n"+xhr.responseText)
			translated_message = JSON.parse(xhr.responseText)["data"]["translations"][0]["translatedText"];
			
			aux["type"] = "message";
			aux["data"]["group"] = msg["data"]["group"];
			aux["data"]["members"] = members;
			aux["data"]["to"] = user["user"];
			aux["data"]["from"] =msg["data"]["from"]["user"];
			aux["data"]["text"] = translated_message;
			
			console.log("\n\nmessage out:");
			console.log(aux);
			messages["translated_messages"].push(aux);
		}
	}
	else{
		var aux = {};
		aux["data"] = {};
		
		var message_to_translate = unescape(msg["data"]["text"]);
		language_from = msg["data"]["from"]["language"]
		language_to = msg["data"]["to"][0]["language"]

		var options = {
			q: message_to_translate,
			source: language_from,
			target: language_to,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer GOOGLE_TRANSLATE_ACCESS_TOKEN'
			}
		}

		option = JSON.stringify(options);
		xhr.open('POST', 'https://translation.googleapis.com/language/translate/v2?key=API_KEY_TRANSLATER',false);
		xhr.send(option);

		translated_message = JSON.parse(xhr.responseText)["data"]["translations"][0]["translatedText"];
		
		aux["type"] = "message";
		aux["data"]["group"] = msg["data"]["group"];
		aux["data"]["to"] = msg["data"]["to"][0]["user"];
		aux["data"]["from"] = msg["data"]["from"]["user"];
		aux["data"]["text"] = translated_message;
		
		console.log("\n\nmessage out:");
		console.log(aux);
		messages["translated_messages"].push(aux);
	}

	return messages;
}
		
